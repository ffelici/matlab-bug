function mytest = test(A,B,C)

if A>1
  A=rand(3); % this is a comment
  fprintf('this is a string with a percentage % symbol, not a comment\n'); % this is a comment
  plot(A,A,'linewidth',2);
  fprintf('this is another string with a % symbol')
end

end
